I made a bullet hell style game in the form of naval combat.

The controls for the game are:
- W: to start moving forward (don't have to hold)
- S: to stop moving
- A: to rotate counter-clockwise
- D: to rotate clockwise
- Q: to shoot on the port side
- E: to shoot on the starboard side
- P: to enter the pause menu

My solution for the final assignment is:
- AI: the enemy ships follow a simplistic AI with some random decisions to decide movement and when to shoot.
- Start screen: The game has a start screen with instructions for starting and quitting the game
- In-play UI: I added a pause menu
