#ifndef _GAME_OBJECT_H_INCLUDED_
#define _GAME_OBJECT_H_INCLUDED_

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_timer.h>

#include <memory>

class GameObject
{
public:
    GameObject();
    GameObject(SDL_Renderer *ren);

    void obj_init();
    void obj_init(const char *sprite_path);
    void obj_init(const char *sprite_path, int x, int y, int w, int h);
    void obj_quit();

    SDL_Rect get_rect();

    //Virtual functions
    virtual void obj_render();
    virtual void obj_update();
    virtual void obj_collision(GameObject *collider);

protected:
    SDL_Renderer *obj_renderer;
    SDL_Texture *obj_graphic;
    SDL_Rect obj_rect;
};

#endif