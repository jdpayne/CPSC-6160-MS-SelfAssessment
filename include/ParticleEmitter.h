#ifndef _PARTICLE_EMITTER_H_INCLUDED_
#define _PARTICLE_EMITTER_H_INCLUDED_

class Particle;

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_timer.h>

#include <vector>

class ParticleEmitter
{
public:
    ParticleEmitter(SDL_Renderer *ren);

    void pe_update();
    void pe_draw();
    void add_particle(Particle *particle);
    void add_particle(int x, int y, int x_vel, int y_vel, int lifetime);
    void add_particle(int x, int y, int x_vel, int y_vel, int w, int h, int lifetime);

private:
    std::vector<Particle *> particles;
    void prune_particles();
    SDL_Renderer *ren;
    const long unsigned int MAX_PARTICLES = 512;
};

#endif