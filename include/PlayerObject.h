#ifndef _PLAYER_OBJECT_H_INCLUDED_
#define _PLAYER_OBJECT_H_INCLUDED_

class Sprite;
class ParticleEmitter;
class CannonBall;
class SpriteManager;

#include "GameObject.h"

class PlayerObject : public GameObject
{
public:
    PlayerObject(SDL_Renderer *ren);

    void obj_update();
    void obj_render();

    void obj_collision(GameObject *collider);

    // control methods
    void control_rotate_port();
    void control_rotate_starboard();
    void control_set_velocity(int vel);
    void control_boost();
    CannonBall *control_shoot_port();
    CannonBall *control_shoot_starboard();

    // TODO: health and death
    bool get_active();

    enum health
    {
        sunk,
        low,
        mid,
        high,
        full
    };
    
    health get_health();

private:
    Sprite *high_sprite;
    Sprite *mid_sprite;
    Sprite *low_sprite;
    Sprite *sunk_sprite;

    void player_keep_in_bounds();
    bool active = true;

    int state_tick_duration;
    int state_tick_start;
    int last_tick_update;

    const int obj_max_vel = 4;
    int obj_vel = 0;

    int theta;

    enum boost_states
    {
        ready,
        boosting,
        recharging
    };
    boost_states boost_state;

    health player_health;

    ParticleEmitter *pe;

    // reloading
    int port_shoot_delay = 1000;
    int port_shoot_start_tick;
    int port_shoot_last_tick;

    int starboard_shoot_delay = 1000;
    int starboard_shoot_start_tick;
    int starboard_shoot_last_tick;

    // need full double accuracy for PI due to
    // weird bug with drifting at wrong angles
    const double PI = 3.14159265358979311600;
    const int cb_speed = 5;
    const int reload_time = 1000;
};

#endif