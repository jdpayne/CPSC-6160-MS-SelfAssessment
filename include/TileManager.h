#ifndef _TILE_MANAGER_H_INCLUDED_
#define _TILE_MANAGER_H_INCLUDED_

#include "GameObject.h"

class TileManager : public GameObject
{
    // location, sprite (rect, texture), size, Tiles (file pointer), render
    // screen dimensions
public:
    TileManager();

    void index_source_sheet(const char *sprite_sheet_path, int num_tiles_width, int num_tiles_height, int tile_width, int tile_height, SDL_Renderer *ren);
    SDL_Texture *get_sheet();
    SDL_Rect *get_tile(int index);

protected:
    SDL_Texture *source_tiles;
    int tile_width;
    int tile_height;
    int num_tiles_x;
    int num_tiles_y;
};

#endif