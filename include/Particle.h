#ifndef _PARTICLE_H_INCLUDED_
#define _PARTICLE_H_INCLUDED_

class SDL_Renderer;

#include <SDL2/SDL.h>

class Particle
{
public:
    Particle(SDL_Renderer *ren);

    void init(int x, int y, int xvel, int yvel, int lifetime);
    void init(int x, int y, int xvel, int yvel, int w, int h, int lifetime);
    void particle_update();
    void particle_draw();
    int get_lifetime();

private:
    SDL_Renderer *ren;
    SDL_Rect rect;
    int x_vel;
    int y_vel;
    int lifetime;
    int last_tick;
};

#endif