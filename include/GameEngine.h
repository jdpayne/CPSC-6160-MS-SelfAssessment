#ifndef _GAME_ENGINE_H_INCLUDED_
#define _GAME_ENGINE_H_INCLUDED_

class PlayerObject;
class GameObject;
class TileManager;
class CollisionDetection;
class EnemyBoat;
class CannonBall;

#include <SDL2/SDL.h>
#include <SDL_ttf.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_timer.h>

#include <vector>

const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;

class GameEngine
{
public:
    GameEngine();
    ~GameEngine();
    void init();
    void load_map_tiles(const char *file_path, int map_width, int map_height);

    void handleEvents_Paused();
    void handleEvents_MainMenu();
    //void render_paused();

    void handleEvents();
    void updateMechanics();
    void render();
    void quit();
    bool get_game_is_running();
    bool get_game_is_paused();
    bool get_game_is_started();
    void handle_player_input(SDL_Event game_event);

protected:
    TTF_Font *game_font;

    void RenderBackground();
    void bg_init();

    void check_and_handle_collisions();

    bool game_is_running;
    bool game_is_paused;
    bool game_is_started;

    SDL_Window *my_window;
    SDL_Renderer *my_renderer;
    SDL_Event input;
    PlayerObject *player;
    std::vector<int> map_tiles;
    std::vector<SDL_Rect> map_rects;
    int map_tile_width;
    int map_tile_height;
    int map_width;  // in tiles
    int map_height; // in tiles
    TileManager *tm;
    CollisionDetection *cd;
    std::vector<CannonBall *> player_projectiles;
    std::vector<CannonBall *> enemy_projectiles;

    std::vector<EnemyBoat *> enemies;
    const int MAX_ENEMIES = 5;
};

#endif