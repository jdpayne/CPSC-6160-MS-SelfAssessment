#ifndef _CANNON_BALL_H_INCLUDED_
#define _CANNON_BALL_H_INCLUDED_

#include "GameObject.h"

class CannonBall : public GameObject
{
public:
    CannonBall(SDL_Renderer *ren);

    void cannon_ball_init(int x_vel, int y_vel, GameObject *parent);
    void cannon_ball_set_bounds(int lower_x, int upper_x, int lower_y, int upper_y);
    void cannon_ball_check_bounds();

    void obj_update();

    GameObject *get_parent();
    void obj_collision(GameObject *collider);

    bool get_active();

    // TODO: destruction

private:
    int x_vel;
    int y_vel;
    GameObject *parent;

    bool active = true;

    // bounds
    int lower_x;
    int upper_x;
    int lower_y;
    int upper_y;
};

#endif