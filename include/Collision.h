#ifndef _COLLISION_H_INCLUDED_
#define _COLLISION_H_INCLUDED_

class GameObject;

class CollisionDetection
{
public:
    CollisionDetection();
    
    bool detect_collision_square(GameObject l, GameObject r);
};

#endif