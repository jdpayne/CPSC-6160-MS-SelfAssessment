#ifndef _ENEMY_BOAT_H_INCLUDED_
#define _ENEMY_BOAT_H_INLCUDED_

class Sprite;
class CannonBall;

#include "GameObject.h"
#include <vector>

class EnemyBoat : public GameObject
{
public:
    EnemyBoat(SDL_Renderer *ren);

    void obj_update();
    void keep_in_bounds();
    void obj_collision(GameObject *collider);
    void obj_render();

    void set_target(GameObject *target);
    void set_projectiles(std::vector<CannonBall *> *v);

    bool get_active();

    enum health
    {
        sunk,
        mid,
        full
    };
    enum states
    {
        move,
        aim,
    };
    enum turns
    {
        left = -1,
        straight,
        right
    };
    enum target_quadrants
    {
        up_left,
        up_right,
        down_left,
        down_right
    };
    // TODO: health and death
private:
    // need full double accuracy for PI due to
    // weird bug with drifting at wrong angles
    const double PI = 3.14159265358979311600;

    Sprite *sunk_sprite;
    Sprite *mid_sprite;

    bool active = true;

    int theta;
    int obj_vel;
    GameObject *target;
    std::vector<CannonBall *> *projectiles;

    health enemy_health;
    states state;
    turns turn;

    target_quadrants get_target_quadrant();

    // each state duration, start time, last update
    int aim_state_duration = 1000;
    int aim_state_start_tick;
    int aim_state_last_tick;

    int aim_delay = 1000;
    int aim_delay_start_tick;
    int aim_delay_last_tick;

    int cb_speed = 5;
    void shoot_port();
    void shoot_starboard();
};

#endif