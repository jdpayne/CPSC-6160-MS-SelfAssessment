#ifndef _SPRITE_MANAGER_H_INLCUDED_
#define _SPRITE_MANAGER_H_INCLUDED_

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_timer.h>

class Sprite
{
public:
    Sprite();

    void sprite_init(SDL_Renderer *ren, const char *sprite_path, int duration, int frames, int width, int height);
    void sprite_start_anim(int tick);
    SDL_Rect *get_anim_frame();
    SDL_Texture *get_sheet();

protected:
    SDL_Renderer *sprite_renderer;
    SDL_Texture *sprite_sheet;
    SDL_Rect *current_frame;
    int animation_start_tick;
    int curr_duration;
    int last_tick;
    int animation_durtation;
    int animation_number_frames;
    int sprite_width;
    int sprite_height;
    int frame_duration;
};

#endif