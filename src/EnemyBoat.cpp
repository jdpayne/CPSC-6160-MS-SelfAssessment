#include "../include/EnemyBoat.h"
#include "../include/CannonBall.h"
#include "../include/PlayerObject.h"
#include "../include/GameEngine.h"
#include "../include/SpriteManager.h"

#include <iostream>
#include <stdlib.h>
#include <typeinfo>
#include <string>
#include <math.h>

int check_distances(SDL_Rect l, SDL_Rect r)
{
    return std::sqrt((std::pow(l.x - r.x, 2) + std::pow(l.y - r.y, 2)));
}

EnemyBoat::turns get_turn(EnemyBoat::target_quadrants t_q, int theta)
{

    int right_chance = 33;
    int left_chance = 66;
    int base_chance_change = 20;
    switch (t_q)
    {
    case EnemyBoat::up_left:
        if (theta <= 90)
        {
            // turn left up
            right_chance -= base_chance_change; // 13
            left_chance += base_chance_change;  // 86 -> straight: 14
        }
        else if (theta < 180)
        {
            // straight up
            right_chance -= base_chance_change;      // 13
            left_chance -= (base_chance_change * 2); // 26 -> straight: 74
        }
        else if (theta < 270)
        {
            // turn right up
            right_chance += (base_chance_change * 2); // 73
            left_chance += base_chance_change;        // 86 -> straight: 14
        }
        else
        {
            // turn left up
            right_chance -= base_chance_change; // 13
            left_chance += base_chance_change;  // 86 -> straight: 14
        }
        break;
    case EnemyBoat::up_right:
        if (theta < 90)
        {
            // straight up
            right_chance -= base_chance_change;      // 13
            left_chance -= (base_chance_change * 2); // 26
        }
        else if (theta <= 180)
        {
            // turn right up
            right_chance += (base_chance_change * 2); // 53
            left_chance += base_chance_change;        // 76
        }
        else if (theta <= 270)
        {
            right_chance += (base_chance_change * 2); // 53
            left_chance += base_chance_change;
        }
        else
        {
            // turn left up
            right_chance -= base_chance_change; // 23
            left_chance += base_chance_change;  // 76
        }
        break;
    case EnemyBoat::down_left:
        if (theta <= 90)
        {
            right_chance += (base_chance_change * 2); // 53
            left_chance += base_chance_change;
        }
        else if (theta <= 180)
        {
            // turn left up
            right_chance -= base_chance_change; // 23
            left_chance += base_chance_change;  // 76
        }
        else if (theta < 270)
        {
            // straight up
            right_chance -= base_chance_change;      // 23
            left_chance -= (base_chance_change * 2); // 46
        }
        else
        {
            // turn right up
            right_chance += (base_chance_change * 2); // 53
            left_chance += base_chance_change;        // 76
        }
        break;
    case EnemyBoat::down_right:
        if (theta < 90)
        {
            // turn right up
            right_chance += (base_chance_change * 2); // 53
            left_chance += base_chance_change;        // 76
        }
        else if (theta < 270)
        {
            // turn left up
            right_chance -= base_chance_change; // 23
            left_chance += base_chance_change;  // 76
        }
        else
        {
            // straight up
            right_chance -= base_chance_change;      // 23
            left_chance -= (base_chance_change * 2); // 46
        }
        break;
    }
    int rand_num = rand() % 100;

    EnemyBoat::turns turn;
    if (rand_num <= right_chance)
    {
        turn = EnemyBoat::right;
    }
    else if (rand_num <= left_chance)
    {
        turn = EnemyBoat::left;
    }
    else
    {
        turn = EnemyBoat::straight;
    }
    return turn;
}

EnemyBoat::EnemyBoat(SDL_Renderer *ren)
{
    this->obj_renderer = ren;
    theta = 0;
    std::srand(time(NULL));
    obj_vel = 3;

    enemy_health = full;
    state = move;
    turn = straight;

    aim_delay_last_tick = aim_state_last_tick = aim_delay_start_tick = aim_state_start_tick = SDL_GetTicks();

    sunk_sprite = new Sprite();
    sunk_sprite->sprite_init(ren, "sprites/EnemyBoat_Sunk.png", 2000, 5, 64, 64);
    mid_sprite = new Sprite();
    mid_sprite->sprite_init(ren, "sprites/EnemyBoat_Low.png", 1000, 10, 64, 64);
}

void EnemyBoat::set_target(GameObject *target)
{
    this->target = target;
}

void EnemyBoat::set_projectiles(std::vector<CannonBall *> *v)
{
    projectiles = v;
}

void EnemyBoat::obj_update()
{
    // states: move, aim, shoot
    // move -> move or move -> aim
    // aim (some duration tbd) -> shoot
    // shoot -> aim or shoot -> move

    if (enemy_health != sunk)
    {
        switch (state)
        {
        case move:
        {
            aim_delay_last_tick = SDL_GetTicks();
            theta += (15 * get_turn(get_target_quadrant(), theta));
            obj_rect.x += obj_vel * cos(theta * PI / 180);
            obj_rect.y += obj_vel * sin(theta * PI / 180);
            keep_in_bounds();

            if (aim_delay_last_tick - aim_delay_start_tick >= aim_delay)
            {
                int aim_chance = 5;

                if (check_distances(obj_rect, target->get_rect()) < 200)
                {
                    aim_chance = 75;
                }
                int rand_num = rand() % 100;
                if (rand_num < aim_chance)
                {
                    state = aim;
                    aim_state_start_tick = aim_state_last_tick = SDL_GetTicks();
                }
            }

            break;
        }
        case aim:
            aim_state_last_tick = SDL_GetTicks();

            switch (get_target_quadrant())
            {
            case up_left:
                theta = 135;
                break;
            case up_right:
                theta = 45;
                break;
            case down_left:
                theta = 225;
                break;
            case down_right:
                theta = 315;
                break;
            }

            if (aim_state_last_tick - aim_state_start_tick >= aim_state_duration)
            {
                shoot_port();
                shoot_starboard();
                state = move;
                aim_delay_start_tick = aim_delay_last_tick = SDL_GetTicks();
            }
            break;
        }
    }
}

void EnemyBoat::keep_in_bounds()
{
    obj_rect.x = std::max(0, std::min(SCREEN_WIDTH - obj_rect.w, obj_rect.x));
    obj_rect.y = std::max(0, std::min(SCREEN_HEIGHT - obj_rect.h, obj_rect.y));
}

void EnemyBoat::obj_render()
{
    switch (enemy_health)
    {
    case sunk:
        SDL_RenderCopyEx(obj_renderer, sunk_sprite->get_sheet(), sunk_sprite->get_anim_frame(), &obj_rect, theta, NULL, SDL_FLIP_NONE);
        break;
    case mid:
        SDL_RenderCopyEx(obj_renderer, mid_sprite->get_sheet(), mid_sprite->get_anim_frame(), &obj_rect, theta, NULL, SDL_FLIP_NONE);
        break;
    default:
        SDL_RenderCopyEx(obj_renderer, obj_graphic, NULL, &obj_rect, theta, NULL, SDL_FLIP_NONE);
        break;
    }
}

void EnemyBoat::obj_collision(GameObject *collider)
{
    CannonBall *cb = dynamic_cast<CannonBall *>(collider);
    if (cb != nullptr)
    {
        enemy_health = health(enemy_health - 1);
        if (enemy_health == sunk)
        {
            active = false;
        }
        // particle effects
        return;
    }

    PlayerObject *po = dynamic_cast<PlayerObject *>(collider);
    if (po != nullptr)
    {
        // bounce
        keep_in_bounds();
        return;
    }

    EnemyBoat *eb = dynamic_cast<EnemyBoat *>(collider);
    if (eb != nullptr)
    {
        // bounce
        keep_in_bounds();
        return;
    }

    // TODO:
    // Player->CannonBall : damage
    // if EnemyBoat : bounce
}

EnemyBoat::target_quadrants EnemyBoat::get_target_quadrant()
{
    if (obj_rect.y > target->get_rect().y)
    {
        if (obj_rect.x > target->get_rect().x)
        {
            return up_left;
        }
        else
        {
            return up_right;
        }
    }
    else if (obj_rect.y < target->get_rect().y)
    {
        if (obj_rect.x > target->get_rect().x)
        {
            return down_left;
        }
        else
        {
            return down_right;
        }
    }
    return down_right;
}

void EnemyBoat::shoot_port()
{
    CannonBall *temp = new CannonBall(obj_renderer);
    int port_theta = theta + 90;
    int obj_x = (obj_rect.x + (0.5 * obj_rect.w)) - (16 * cos(port_theta * PI / 180));
    int obj_y = (obj_rect.y + (0.5 * obj_rect.h)) - (16 * sin(port_theta * PI / 180));
    temp->obj_init("sprites/CannonBall.png", obj_x, obj_y, 5, 5);

    int obj_x_vel = cb_speed * cos(port_theta * PI / 180);
    int obj_y_vel = cb_speed * sin(port_theta * PI / 180);
    temp->cannon_ball_init(obj_x_vel, obj_y_vel, this);
    temp->cannon_ball_set_bounds(0, SCREEN_WIDTH, 0, SCREEN_HEIGHT);
    projectiles->push_back(temp);
}

void EnemyBoat::shoot_starboard()
{
    CannonBall *temp = new CannonBall(obj_renderer);

    int starboard_theta = theta - 90;
    int obj_x = (obj_rect.x + (0.5 * obj_rect.w)) - (16 * cos(starboard_theta * PI / 180));
    int obj_y = (obj_rect.y + (0.5 * obj_rect.h)) - (16 * sin(starboard_theta * PI / 180));
    temp->obj_init("sprites/CannonBall.png", obj_x, obj_y, 5, 5);

    int obj_x_vel = cb_speed * cos(starboard_theta * PI / 180);
    int obj_y_vel = cb_speed * sin(starboard_theta * PI / 180);
    temp->cannon_ball_init(obj_x_vel, obj_y_vel, this);
    temp->cannon_ball_set_bounds(0, SCREEN_WIDTH, 0, SCREEN_HEIGHT);
    projectiles->push_back(temp);
}

bool EnemyBoat::get_active()
{
    return active;
}