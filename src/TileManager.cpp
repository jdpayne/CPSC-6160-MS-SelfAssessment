#include "../include/TileManager.h"

TileManager::TileManager()
{
}

void TileManager::index_source_sheet(const char *file_path, int num_tiles_width, int num_tiles_height, int tile_width, int tile_height, SDL_Renderer *ren)
{
    source_tiles = SDL_CreateTextureFromSurface(ren, IMG_Load(file_path));
    this->tile_width = tile_width;
    this->tile_height = tile_height;
    num_tiles_x = num_tiles_width;
    num_tiles_y = num_tiles_height;
}

SDL_Rect *TileManager::get_tile(int index)
{
    SDL_Rect *temp = new SDL_Rect();
    temp->w = tile_width;
    temp->h = tile_height;
    temp->x = (index % num_tiles_x) * tile_width;
    temp->y = (index % num_tiles_y) * tile_height;
    return temp;
}

SDL_Texture *TileManager::get_sheet()
{
    return source_tiles;
}