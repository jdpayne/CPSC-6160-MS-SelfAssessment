#include "../include/Particle.h"

#include <iostream>
#include <string>
#include <fstream>

#include <SDL2/SDL_image.h>
#include <SDL2/SDL_timer.h>

Particle::Particle(SDL_Renderer *ren)
{
    this->ren = ren;
}

void Particle::init(int x, int y, int xvel, int yvel, int lifetime)
{
    rect.x = x;
    rect.y = y;
    x_vel = xvel;
    y_vel = yvel;
    this->lifetime = lifetime;
    rect.h = 20;
    rect.w = 20;
    last_tick = SDL_GetTicks();
}

void Particle::init(int x, int y, int xvel, int yvel, int w, int h, int lifetime)
{
    rect.x = x;
    rect.y = y;
    x_vel = xvel;
    y_vel = yvel;
    this->lifetime = lifetime;
    rect.h = h;
    rect.w = w;
    last_tick = SDL_GetTicks();
}

void Particle::particle_update()
{
    rect.x += x_vel;
    rect.y += y_vel;

    //update y_vel, x_vel

    lifetime -= SDL_GetTicks() - last_tick;
    last_tick = SDL_GetTicks();
}

void Particle::particle_draw()
{
    // render stuff
    SDL_SetRenderDrawColor(ren, 123, 123, 123, 255);
    SDL_RenderFillRect(ren, &rect);
}

int Particle::get_lifetime()
{
    return lifetime;
}
