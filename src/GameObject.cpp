#include "../include/GameObject.h"
#include "../include/SpriteManager.h"

#include <iostream>
#include <string>
#include <fstream>

GameObject::GameObject() {}

GameObject::GameObject(SDL_Renderer *ren)
{
    obj_renderer = ren;
}

void GameObject::obj_init() {}

void GameObject::obj_init(const char *sprite_path)
{
    obj_graphic = SDL_CreateTextureFromSurface(obj_renderer, IMG_Load(sprite_path));
}

void GameObject::obj_init(const char *sprite_path, int x, int y, int w, int h)
{
    obj_graphic = SDL_CreateTextureFromSurface(obj_renderer, IMG_Load(sprite_path));
    obj_rect.x = x;
    obj_rect.y = y;
    obj_rect.w = w;
    obj_rect.h = h;
}

void GameObject::obj_update() {}

void GameObject::obj_render()
{
    SDL_RenderCopy(obj_renderer, obj_graphic, NULL, &obj_rect);
}

void GameObject::obj_quit() {}

SDL_Rect GameObject::get_rect()
{
    return obj_rect;
}

void GameObject::obj_collision(GameObject *collider) {}