#include "../include/PlayerObject.h"
#include "../include/SpriteManager.h"
#include "../include/Particle.h"
#include "../include/ParticleEmitter.h"
#include "../include/GameEngine.h"
#include "../include/CannonBall.h"
#include "../include/EnemyBoat.h"

#include <math.h>
#include <iostream>

PlayerObject::PlayerObject(SDL_Renderer *ren)
{
    obj_renderer = ren;
    obj_rect.w = 64;
    obj_rect.h = 64;
    obj_rect.y = 240;
    obj_rect.x = 320;
    state_tick_duration = 0;

    boost_state = ready;
    player_health = full;

    pe = new ParticleEmitter(ren);
    theta = 0;

    sunk_sprite = new Sprite();
    sunk_sprite->sprite_init(ren, "sprites/PlayerBoat_Sunk.png", 1000, 10, 64, 64);
    low_sprite = new Sprite();
    low_sprite->sprite_init(ren, "sprites/PlayerBoat_Low.png", 1000, 10, 64, 64);
    mid_sprite = new Sprite();
    mid_sprite->sprite_init(ren, "sprites/PlayerBoat_Mid.png", 1000, 10, 64, 64);
    high_sprite = new Sprite();
    high_sprite->sprite_init(ren, "sprites/PlayerBoat_High.png", 1000, 10, 64, 64);
    port_shoot_start_tick = starboard_shoot_start_tick = SDL_GetTicks();
}

void PlayerObject::obj_update()
{
    port_shoot_last_tick = starboard_shoot_last_tick = SDL_GetTicks();

    pe->pe_update();
    int boost = 1;
    if (boost_state == boosting)
    {
        boost = 10;
        boost_state = recharging;
    }

    obj_rect.x += obj_vel * cos(theta * PI / 180) * boost;
    obj_rect.y += obj_vel * sin(theta * PI / 180) * boost;
    player_keep_in_bounds();
}

void PlayerObject::player_keep_in_bounds()
{
    obj_rect.x = std::max(0, std::min(SCREEN_WIDTH - obj_rect.w, obj_rect.x));
    obj_rect.y = std::max(0, std::min(SCREEN_HEIGHT - obj_rect.h, obj_rect.y));
}

void PlayerObject::obj_render()
{
    // SDL_SetRenderDrawColor(obj_renderer, 255, 255, 255, 255);
    // SDL_RenderFillRect(obj_renderer, &obj_rect);
    switch (player_health)
    {
    case sunk:
        SDL_RenderCopyEx(obj_renderer, sunk_sprite->get_sheet(), sunk_sprite->get_anim_frame(), &obj_rect, theta, NULL, SDL_FLIP_NONE);
        break;
    case low:
        SDL_RenderCopyEx(obj_renderer, low_sprite->get_sheet(), low_sprite->get_anim_frame(), &obj_rect, theta, NULL, SDL_FLIP_NONE);
        break;
    case mid:
        SDL_RenderCopyEx(obj_renderer, mid_sprite->get_sheet(), mid_sprite->get_anim_frame(), &obj_rect, theta, NULL, SDL_FLIP_NONE);
        break;
    case high:
        SDL_RenderCopyEx(obj_renderer, high_sprite->get_sheet(), high_sprite->get_anim_frame(), &obj_rect, theta, NULL, SDL_FLIP_NONE);
        break;
    default:
        SDL_RenderCopyEx(obj_renderer, obj_graphic, NULL, &obj_rect, theta, NULL, SDL_FLIP_NONE);
    }
    pe->pe_draw();
}

void PlayerObject::obj_collision(GameObject *collider)
{
    CannonBall *cb = dynamic_cast<CannonBall *>(collider);
    if (cb != nullptr)
    {
        player_health = health(player_health - 1);
        if (player_health == sunk)
        {
            active = false;
        }
        return;
    }
}

void PlayerObject::control_rotate_port()
{
    theta = (theta - 15) % 360;
}

void PlayerObject::control_rotate_starboard()
{
    theta = (theta + 15) % 360;
}

void PlayerObject::control_set_velocity(int vel)
{
    obj_vel = std::min(obj_max_vel, std::max(vel, 0));
}

void PlayerObject::control_boost()
{
    // TODO: add boost and cooldown
}

CannonBall *PlayerObject::control_shoot_port()
{
    // TODO: reload
    if (port_shoot_last_tick - port_shoot_start_tick >= port_shoot_delay)
    {
        CannonBall *temp = new CannonBall(obj_renderer);
        int port_theta = theta + 90;
        int obj_x = (obj_rect.x + (0.5 * obj_rect.w)) - (16 * cos(port_theta * PI / 180));
        int obj_y = (obj_rect.y + (0.5 * obj_rect.h)) - (16 * sin(port_theta * PI / 180));
        temp->obj_init("sprites/CannonBall.png", obj_x, obj_y, 5, 5);

        int obj_x_vel = cb_speed * cos(port_theta * PI / 180);
        int obj_y_vel = cb_speed * sin(port_theta * PI / 180);
        temp->cannon_ball_init(obj_x_vel, obj_y_vel, this);
        temp->cannon_ball_set_bounds(0, SCREEN_WIDTH, 0, SCREEN_HEIGHT);
        port_shoot_last_tick = port_shoot_start_tick = SDL_GetTicks();
        return temp;
    }
    else
    {
        return nullptr;
    }
}

CannonBall *PlayerObject::control_shoot_starboard()
{
    // TODO: reload
    if (starboard_shoot_last_tick - starboard_shoot_start_tick >= starboard_shoot_delay)
    {
        CannonBall *temp = new CannonBall(obj_renderer);

        int starboard_theta = theta - 90;
        int obj_x = (obj_rect.x + (0.5 * obj_rect.w)) - (16 * cos(starboard_theta * PI / 180));
        int obj_y = (obj_rect.y + (0.5 * obj_rect.h)) - (16 * sin(starboard_theta * PI / 180));
        temp->obj_init("sprites/CannonBall.png", obj_x, obj_y, 5, 5);

        int obj_x_vel = cb_speed * cos(starboard_theta * PI / 180);
        int obj_y_vel = cb_speed * sin(starboard_theta * PI / 180);
        temp->cannon_ball_init(obj_x_vel, obj_y_vel, this);
        temp->cannon_ball_set_bounds(0, SCREEN_WIDTH, 0, SCREEN_HEIGHT);
        starboard_shoot_last_tick = starboard_shoot_start_tick = SDL_GetTicks();
        return temp;
    }
    else
    {
        return nullptr;
    }
}

bool PlayerObject::get_active()
{
    return active;
}

PlayerObject::health PlayerObject::get_health()
{
    return player_health;
}