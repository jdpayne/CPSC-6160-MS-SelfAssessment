#include "../include/CannonBall.h"
#include <iostream>

CannonBall::CannonBall(SDL_Renderer *ren)
{
    obj_renderer = ren;
}

void CannonBall::cannon_ball_init(int xv, int yv, GameObject *p)
{
    x_vel = xv;
    y_vel = yv;
    parent = p;
}

void CannonBall::cannon_ball_set_bounds(int lx, int ux, int ly, int uy)
{
    lower_x = lx - (obj_rect.w * 2);
    upper_x = ux + (obj_rect.w * 2);
    lower_y = ly - (obj_rect.h * 2);
    upper_y = uy + (obj_rect.h * 2);
}

void CannonBall::obj_update()
{
    obj_rect.x -= x_vel;
    obj_rect.y -= y_vel;

    // TODO: add delete when out of bounds
}

GameObject *CannonBall::get_parent()
{
    return parent;
}

void CannonBall::obj_collision(GameObject *collider)
{
    // destroy
    active = false;
}

bool CannonBall::get_active()
{
    return active;
}