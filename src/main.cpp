#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_timer.h>

#include "../include/GameEngine.h"

const int fps = 60;
const int frame_duration = 1 + 1000 / fps;

int main()
{
  GameEngine *ge = new GameEngine();

  ge->init();
  ge->load_map_tiles("map_data.txt", 16, 12);

  int this_start_time;
  int this_duration;
  while (ge->get_game_is_running())
  {
    this_start_time = SDL_GetTicks();
    if (!ge->get_game_is_started())
    {
      ge->handleEvents_MainMenu();
      ge->render();
    }
    else
    {
      if (!ge->get_game_is_paused())
      {
        ge->handleEvents();
        ge->updateMechanics();
        ge->render();
      }
      else
      {
        ge->handleEvents_Paused();
        ge->render();
      }
    }

    this_duration = SDL_GetTicks() - this_start_time;
    if (this_duration < frame_duration)
    {
      SDL_Delay(frame_duration - this_duration);
    }
  }

  ge->quit();

  return 0;
}
