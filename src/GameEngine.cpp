#include "../include/GameEngine.h"
#include "../include/PlayerObject.h"
#include "../include/TileManager.h"
#include "../include/Collision.h"
#include "../include/EnemyBoat.h"
#include "../include/CannonBall.h"

#include <iostream>
#include <string>
#include <fstream>

GameEngine::GameEngine()
{
  game_is_running = true;
  game_is_paused = false;
  game_is_started = false;
  map_tile_height = 40;
  map_tile_width = 40;
}

void GameEngine::init()
{

  if (SDL_Init(SDL_INIT_EVERYTHING) != 0)
  {
    std::cout << "Error initializing SDL: " << SDL_GetError() << std::endl;
  }

  //Enable gpu_enhanced textures
  IMG_Init(IMG_INIT_PNG);

  my_window = SDL_CreateWindow("my_game",
                               SDL_WINDOWPOS_CENTERED,
                               SDL_WINDOWPOS_CENTERED,
                               SCREEN_WIDTH,
                               SCREEN_HEIGHT, 0);
  my_renderer = SDL_CreateRenderer(my_window, -1, 0);

  player = new PlayerObject(my_renderer);
  player->obj_init("sprites/PlayerBoat.png");

  tm = new TileManager();
  tm->index_source_sheet("sprites/bad_wave.png", 2, 1, 40, 40, my_renderer);

  cd = new CollisionDetection();

  enemies.push_back(new EnemyBoat(my_renderer));
  enemies.at(0)->obj_init("sprites/EnemyBoat.png", 0, 0, 64, 64);
  enemies.at(0)->set_target(player);
  enemies.at(0)->set_projectiles(&enemy_projectiles);

  enemies.push_back(new EnemyBoat(my_renderer));
  enemies.at(1)->obj_init("sprites/EnemyBoat.png", 600, 440, 64, 64);
  enemies.at(1)->set_target(player);
  enemies.at(1)->set_projectiles(&enemy_projectiles);

  bg_init();

  TTF_Init();
  game_font = TTF_OpenFont("ChrustyRock-ORLA.ttf", 50);
}

void GameEngine::bg_init()
{
}

void GameEngine::quit()
{

  SDL_DestroyRenderer(my_renderer);
  SDL_DestroyWindow(my_window);

  IMG_Quit();
  SDL_Quit();
}

void GameEngine::handleEvents()
{
  SDL_Event game_event;
  SDL_PollEvent(&game_event);

  switch (game_event.type)
  {
  case SDL_QUIT:
    game_is_running = false;
    break;
  case SDL_KEYDOWN:
    if (player->get_active())
    {
      handle_player_input(game_event);
    }
    break;
  }
}

void GameEngine::handleEvents_Paused()
{
  SDL_Event game_event;
  SDL_PollEvent(&game_event);

  switch (game_event.type)
  {
  case SDL_QUIT:
    game_is_running = false;
    break;
  case SDL_KEYDOWN:
    if (game_event.key.keysym.sym == SDLK_p)
    {
      game_is_paused = false;
    }
    else if (game_event.key.keysym.sym == SDLK_ESCAPE)
    {
      game_is_paused = false;
      game_is_running = false;
    }
    break;
  }
}

void GameEngine::handleEvents_MainMenu()
{
  SDL_Event game_event;
  SDL_PollEvent(&game_event);

  switch (game_event.type)
  {
  case SDL_QUIT:
    game_is_running = false;
    break;
  case SDL_KEYDOWN:
    if (game_event.key.keysym.sym == SDLK_RETURN)
    {
      game_is_started = true;
    }
    else if (game_event.key.keysym.sym == SDLK_ESCAPE)
    {
      game_is_running = false;
    }
    break;
  }
}

void GameEngine::updateMechanics()
{
  for (auto boat : enemies)
  {
    boat->obj_update();
  }

  for (auto cannonball : player_projectiles)
  {
    cannonball->obj_update();
  }

  for (auto cannonball : enemy_projectiles)
  {
    cannonball->obj_update();
  }

  if (player->get_active())
  {
    player->obj_update();
  }
  check_and_handle_collisions();
}

void GameEngine::render()
{
  SDL_SetRenderDrawColor(my_renderer, 0, 157, 196, 255);
  SDL_RenderClear(my_renderer);

  RenderBackground();

  for (u_int i = 0; i < map_tiles.size(); i++)
  {
    //SDL_RenderCopy(my_renderer, tm->get_sheet(), tm->get_tile(map_tiles.at(i)), &map_rects.at(i));
  }

  for (auto boat : enemies)
  {
    boat->obj_render();
  }

  player->obj_render();

  for (auto cannonball : player_projectiles)
  {
    if (cannonball->get_active())
    {
      cannonball->obj_render();
    }
  }

  for (auto cannonball : enemy_projectiles)
  {
    if (cannonball->get_active())
    {
      cannonball->obj_render();
    }
  }

  if (!game_is_started)
  {
    SDL_Rect main_menu_rect = {SCREEN_WIDTH / 2 - 100, SCREEN_HEIGHT / 2 - 50, 200, 100};
    SDL_Texture *main_menu_texture = SDL_CreateTextureFromSurface(my_renderer, TTF_RenderText_Solid(game_font, "Thar She Blows", {255, 255, 255}));
    SDL_RenderCopy(my_renderer, main_menu_texture, NULL, &main_menu_rect);

    SDL_Rect play_control_rect = {SCREEN_WIDTH / 2 - 75, SCREEN_HEIGHT / 2 + 50, 150, 50};
    SDL_Texture *play_control_texture = SDL_CreateTextureFromSurface(my_renderer, TTF_RenderText_Solid(game_font, "Press Enter to Play", {255, 255, 255}));
    SDL_RenderCopy(my_renderer, play_control_texture, NULL, &play_control_rect);

    SDL_Rect quit_control_rect = {SCREEN_WIDTH / 2 - 75, SCREEN_HEIGHT / 2 + 100, 150, 50};
    SDL_Texture *quit_control_texture = SDL_CreateTextureFromSurface(my_renderer, TTF_RenderText_Solid(game_font, "Press esc to Quit", {255, 255, 255}));
    SDL_RenderCopy(my_renderer, quit_control_texture, NULL, &quit_control_rect);
  }
  else if (game_is_paused)
  {
    SDL_Rect main_menu_rect = {SCREEN_WIDTH / 2 - 100, SCREEN_HEIGHT / 2 - 50, 200, 100};
    SDL_Texture *main_menu_texture = SDL_CreateTextureFromSurface(my_renderer, TTF_RenderText_Solid(game_font, "Paused", {255, 255, 255}));
    SDL_RenderCopy(my_renderer, main_menu_texture, NULL, &main_menu_rect);

    SDL_Rect play_control_rect = {SCREEN_WIDTH / 2 - 75, SCREEN_HEIGHT / 2 + 50, 150, 50};
    SDL_Texture *play_control_texture = SDL_CreateTextureFromSurface(my_renderer, TTF_RenderText_Solid(game_font, "Press p to Resume", {255, 255, 255}));
    SDL_RenderCopy(my_renderer, play_control_texture, NULL, &play_control_rect);

    SDL_Rect quit_control_rect = {SCREEN_WIDTH / 2 - 75, SCREEN_HEIGHT / 2 + 100, 150, 50};
    SDL_Texture *quit_control_texture = SDL_CreateTextureFromSurface(my_renderer, TTF_RenderText_Solid(game_font, "Press esc to Quit", {255, 255, 255}));
    SDL_RenderCopy(my_renderer, quit_control_texture, NULL, &quit_control_rect);
  }

  SDL_RenderPresent(my_renderer);
}

void GameEngine::RenderBackground()
{
}

bool GameEngine::get_game_is_running()
{
  return game_is_running;
}

bool GameEngine::get_game_is_paused()
{
  return game_is_paused;
}

bool GameEngine::get_game_is_started()
{
  return game_is_started;
}

void GameEngine::load_map_tiles(const char *file_path, int map_width, int map_height)
{
  std::fstream file;
  file.open(file_path, std::ios::in);
  int t;
  int index = 0;
  while (file >> t)
  {
    SDL_Rect temp_rect;
    temp_rect.x = (index % map_width) * map_tile_width;
    temp_rect.y = (index / map_width) * map_tile_height;
    temp_rect.w = map_tile_width;
    temp_rect.h = map_tile_height;
    map_tiles.push_back(t);
    map_rects.push_back(temp_rect);
    index++;
  }
}

void GameEngine::handle_player_input(SDL_Event game_event)
{
  if (game_event.type == SDL_KEYDOWN)
  {
    switch (game_event.key.keysym.sym)
    {
    case SDLK_p:
      game_is_paused = true;
      break;

    case SDLK_a:
    case SDLK_LEFT:
      player->control_rotate_port();
      break;

    case SDLK_d:
    case SDLK_RIGHT:
      player->control_rotate_starboard();
      break;

    case SDLK_w:
    case SDLK_UP:
      player->control_set_velocity(3);
      break;

    case SDLK_s:
    case SDLK_DOWN:
      player->control_set_velocity(0);
      break;

    case SDLK_SPACE:
      player->control_boost();
      break;

    case SDLK_q:
    {
      CannonBall *temp = player->control_shoot_port();
      if (temp != nullptr)
      {
        player_projectiles.push_back(temp);
      }

      break;
    }
    case SDLK_e:
    {
      CannonBall *temp = player->control_shoot_starboard();
      if (temp != nullptr)
      {
        player_projectiles.push_back(temp);
      }
      break;
    }
    default:
      break;
    }
  }
}

void GameEngine::check_and_handle_collisions()
{
  for (auto boat : enemies)
  {
    // enemies to player
    if (boat->get_active())
    {
      // enemies to player cannons
      for (auto cb : player_projectiles)
      {
        if (cb->get_active())
        {
          if (cd->detect_collision_square(*boat, *cb))
          {
            boat->obj_collision(cb);
            cb->obj_collision(boat);
          }
        }
      }
    }
  }

  for (auto ecb : enemy_projectiles)
  {
    // enemy projectiles to player
    if (ecb->get_active() && player->get_active())
    {
      if (cd->detect_collision_square(*ecb, *player))
      {
        ecb->obj_collision(player);
        player->obj_collision(ecb);
      }
    }
  }
}