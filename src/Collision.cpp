#include "../include/Collision.h"
#include "../include/GameObject.h"

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_timer.h>

#include <iostream>

CollisionDetection::CollisionDetection() {}

bool CollisionDetection::detect_collision_square(GameObject l, GameObject r)
{
    if (l.get_rect().x > r.get_rect().x + r.get_rect().w) return false;

    if (l.get_rect().x + l.get_rect().w < r.get_rect().x) return false;

    if (l.get_rect().y > r.get_rect().y + r.get_rect().h) return false;

    if (l.get_rect().y + l.get_rect().h < r.get_rect().y) return false;

    return true;
}