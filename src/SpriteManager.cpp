#include "../include/SpriteManager.h"

#include <iostream>
#include <string>
#include <fstream>

Sprite::Sprite() {
    current_frame = new SDL_Rect();
}

void Sprite::sprite_init(SDL_Renderer *ren, const char *sprite_path, int duration, int frames, int width, int height)
{
    sprite_sheet = SDL_CreateTextureFromSurface(ren, IMG_Load(sprite_path));
    animation_durtation = duration;
    animation_number_frames = frames;
    frame_duration = duration / frames;
    sprite_width = width;
    sprite_height = height;
    current_frame->w = width;
    current_frame->h = height;
    current_frame->x = 0;
    current_frame->y = 0;
}

void Sprite::sprite_start_anim(int tick)
{
    animation_start_tick = tick;
    last_tick = tick;
    curr_duration = 0;
}

SDL_Rect *Sprite::get_anim_frame()
{
    int curr_tick = SDL_GetTicks();
    curr_duration += (curr_tick - last_tick);
    current_frame->x = ((curr_duration / frame_duration) % animation_number_frames) * sprite_width;
    last_tick = curr_tick;
    return current_frame;
}

SDL_Texture *Sprite::get_sheet(){
    return sprite_sheet;
}