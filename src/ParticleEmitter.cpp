#include "../include/ParticleEmitter.h"
#include "../include/Particle.h"

#include <iostream>
#include <string>
#include <fstream>

ParticleEmitter::ParticleEmitter(SDL_Renderer *ren)
{
    this->ren = ren;
}

void ParticleEmitter::pe_update()
{
    for (auto particle : particles)
    {
        particle->particle_update();
    }
}

void ParticleEmitter::pe_draw()
{
    for (auto particle : particles)
    {
        if (particle->get_lifetime() > 0)
        {
            particle->particle_draw();
        }
    }
}

void ParticleEmitter::add_particle(int x, int y, int x_vel, int y_vel, int lifetime)
{
    if (particles.size() == MAX_PARTICLES)
    {
        prune_particles();
    }
    Particle *temp = new Particle(ren);
    temp->init(x, y, x_vel, y_vel, lifetime);
    particles.push_back(temp);
}

void ParticleEmitter::add_particle(int x, int y, int x_vel, int y_vel, int w, int h, int lifetime)
{
    if (particles.size() == MAX_PARTICLES)
    {
        prune_particles();
    }
    Particle *temp = new Particle(ren);
    temp->init(x, y, x_vel, y_vel, w, h, lifetime);
    particles.push_back(temp);
}

void ParticleEmitter::add_particle(Particle *particle)
{
    if (particles.size() == MAX_PARTICLES)
    {
        prune_particles();
    }
    particles.push_back(particle);
}

void ParticleEmitter::prune_particles()
{
    std::vector<Particle *> tempVec;
    for (auto particle : particles)
    {
        if (particle->get_lifetime() > 0)
        {
            tempVec.push_back(particle);
        }
    }
    particles.swap(tempVec);
}